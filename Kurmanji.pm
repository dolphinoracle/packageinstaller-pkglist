<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Kurmanji
</name>

<description>  
Kurmanji dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
hunspell-kmr
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-kmr
</uninstall_package_names>

</app>
