<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Scottish Gaelic
</name>

<description>  
Scottish Gaelic dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
hunspell-gd
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-gd
</uninstall_package_names>

</app>
