<?xml version="1.0"?>
<app>

<category>
Newsreader
</category>

<name>  
Newsboat
</name>

<description>  
an RSS/Atom feed reader for text terminals
</description>

<installable>
all
</installable>

<preinstall>

</preinstall>

<install_package_names>
newsboat
</install_package_names>


<postinstall>

</postinstall>


<uninstall_package_names>
newsboat
</uninstall_package_names>
</app>