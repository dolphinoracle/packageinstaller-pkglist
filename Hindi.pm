<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Hindi
</name>

<description>  
Hindi dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
hunspell-hi
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-hi
</uninstall_package_names>

</app>
