<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Serbian (Cyrillic)
</name>

<description>  
Serbian (Cyrillic) dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
hunspell-sr
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-sr
</uninstall_package_names>

</app>
