<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
English (Canada)
</name>

<description>  
English (Canada) dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
hunspell-en-ca
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-en-ca
</uninstall_package_names>

</app>
