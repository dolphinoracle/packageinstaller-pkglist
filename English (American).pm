<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
English (American)
</name>

<description>  
English (American) dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
hunspell-en-us
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-en-us
</uninstall_package_names>

</app>
