<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Swiss (German)
</name>

<description>  
Swiss (German) dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
hunspell-de-ch
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-de-ch
</uninstall_package_names>

</app>
