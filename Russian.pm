<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Russian
</name>

<description>  
Russian dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
hunspell-ru
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-ru
</uninstall_package_names>

</app>
