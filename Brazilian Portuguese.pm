<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Brazilian Portuguese
</name>

<description>  
Brazilian Portuguese dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
hunspell-pt-br
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-pt-br
</uninstall_package_names>

</app>
