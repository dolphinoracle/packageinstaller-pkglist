<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Modern Greek
</name>

<description>  
Modern Greek dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
hunspell-el
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-el
</uninstall_package_names>

</app>
