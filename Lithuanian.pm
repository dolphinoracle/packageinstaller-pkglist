<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Lithuanian
</name>

<description>  
Lithuanian dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
hunspell-lt
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-lt
</uninstall_package_names>

</app>
