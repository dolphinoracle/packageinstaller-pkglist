<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Slovak
</name>

<description>  
Slovak dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
hunspell-sk
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-sk
</uninstall_package_names>

</app>
