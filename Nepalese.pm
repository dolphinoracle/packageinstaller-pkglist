<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Nepalese
</name>

<description>  
Nepalese dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
hunspell-ne
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-ne
</uninstall_package_names>

</app>
