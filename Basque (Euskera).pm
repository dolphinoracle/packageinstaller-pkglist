<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>
Basque (Euskera)
</name>

<description>  
Basque (Euskera) dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
hunspell-eu
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-eu
</uninstall_package_names>

</app>
