<?xml version="1.0"?>
<app>

<category>
Language
</category>

<name>  
Classical Tibetan
</name>

<description>  
Classical Tibetan dictionary for hunspell
</description>

<installable>
all
</installable>

<screenshot>none</screenshot>

<preinstall>
echo "---- in preprocessing ----"
echo "---- preprocessing done----"
</preinstall>

<install_package_names>
hunspell-bo
</install_package_names>

<postinstall>

</postinstall>

<uninstall_package_names>
hunspell-bo
</uninstall_package_names>

</app>
